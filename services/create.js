var exec = require('child_process').exec;
var async = require('async');
var pathlib = require('path');
var fs = require('fs');
var ncp = require('ncp').ncp;
var format = require('util').format;
var executeCommand = require('../utls').executeCommand;
var userid = require('userid');
var log = require('../log.js');

function createUser(username, home, group, callback){
    executeCommand("useradd -G "+group+" -m -d "+home+" -s /bin/bash "+username, callback);
}
function createUserWGroup(username, home, callback){
	executeCommand("useradd -m -d "+home+" -s /bin/bash "+username, callback);
}
function createGroupUser(username, home, callback){
	executeCommand("useradd -m -d "+home+" -s /bin/bash "+username, callback);
}
function addUserToGroup(username, group, callback){
    executeCommand("passwd -a "+username+" "+group, callback);
}
function createGroup(group, callback){
    executeCommand("groupadd "+group, callback);
}
function deleteUser(username, callback){
	log.debug("Removing user " + username);
	command = format("deluser --remove-home %s", username);
	log.debug("User Removed");
	// @TODO: Actually remove the server from config.json...
	executeCommand(command, callback)
}


function linkDir(from_path, to_path, callback){
	command = format("cp -s -u -R %s/* %s", from_path, to_path);
	executeCommand(command, callback)
}


function fixperms(user, group, path, callback){
        log.debug("Fixing file permissions");
        executeCommand("chown -R "+ user +":"+ group +" "+ path, callback);
        log.debug("Permissions fixed");
};

function fixpermsforfile(user, group, path, callback){
        log.debug("Fixing file permissions");
        executeCommand("chown "+ user +":"+ group +" "+ path, callback);
        log.debug("Permissions fixed");
};

function modperms(pathdir, callback){
        log.debug("Modyfing permissions (chmod)");
        executeCommand("chmod 771 -R "+ pathdir, callback);
        log.debug("Permissions moded (chmod)");
};


function modpermsforfile(dirrectory, callback){
        log.debug("Modyfing permissions (chmod)");
        executeCommand("chmod 771 "+ dirrectory, callback);
        log.debug("Permissions moded (chmod)");
};

function modpermsforengine(dirrectory, callback){
        log.debug("Modyfing permissions (chmod)");
        executeCommand("chmod 644 "+ dirrectory, callback);
        log.debug("Permissions moded (chmod)");
};


function replaceFiles(base_folder, files, backing_folder, callback){
	finalfiles = [];

	files.forEach(function(file) {
	isWildcard = (file.indexOf("*") != -1);
	if (isWildcard == true){
		glob(file, {'cwd':base_folder, 'sync':true}, function (er, files) {
	finalfiles = finalfiles.concat(files);
		});
	};
	})

	async.each(finalfiles, function( file, icallback) {
		var fileTo = pathlib.join(base_folder, file);
		var fileFrom = pathlib.join(backing_folder, file);

		fs.exists(fileTo, function (exists) {
			if(exists) {
				fs.unlink(fileTo, function(err){
					if (err) icallback();
					ncp(fileFrom, fileTo, function(err){
						if (err) throw err;
						icallback();
					});
				});

			} else {
				icallback();
			}
		});
	}, function(err){
		if( err ) {
			log.error('A file failed to process');
		} else {
			log.info('All files have been processed successfully');
		}
	});
	callback();
}

function symlinkFolder(gameserver, from_path, replacements, parentcallback){
	async.series([
	function(callback) {
		linkDir(from_path, gameserver.config.path, function(){callback(null)});

	},
	function(callback) {
		replaceFiles(gameserver.config.path, replacements, from_path, function cb(){callback(null); });
	}

	], function(err, results){parentcallback();});
}

function copyFolder(from_path , gameserver, parentcallback){
	ncp(from_path, gameserver, function (err) {
	if (err) {
		return console.error(err);
	}
	parentcallback();
	});
}

function copyEngine(from_path, to_path, callback){
  command = format("cp -u %s %s", from_path, to_path);
  executeCommand(command, callback)
}

function createDir(path, callback){
  command = format("mkdir -p %s", path);
  executeCommand(command, callback);
}

exports.copyFolder = copyFolder;
exports.copyEngine = copyEngine;
exports.symlinkFolder = symlinkFolder;
exports.createUser = createUser;
exports.createUserWGroup = createUserWGroup;
exports.createGroup = createGroup;
exports.deleteUser = deleteUser;
exports.fixperms = fixperms;
exports.fixpermsforfile = fixpermsforfile;
exports.modperms = modperms;
exports.modpermsforfile = modpermsforfile;
exports.modpermsforengine = modpermsforengine;
exports.linkDir = linkDir;
exports.createDir = createDir;
exports.addUserToGroup = addUserToGroup;
exports.createGroupUser = createGroupUser;
